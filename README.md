# Batch Processing with HTCondor

## Contents
- [Batch Processing with HTCondor](#batch-processing-with-htcondor)
  - [Contents](#contents)
  - [Docker](#docker)
    - [Dockerfile](#dockerfile)
    - [Building the Image](#building-the-image)
  - [Python Script](#python-script)
  - [Testing Docker Images](#testing-docker-images)
  - [Shell Script](#shell-script)
  - [Submission File](#submission-file)
  - [GPU](#gpu)
  - [Common Issues](#common-issues)
   
----

CERN provides a Batch Processing service - HTCondor - which schedules your code to run across a fleet of Virtual Machines. These has a number of advantages:
- you can run your code on machines more powerful than your laptop/desktop
- you can use a Linux VM to run code developed on a Mac/Windows PC
- you can request access to GPUs to accelerate some operations
- you can use Docker containers to run your code in identical environments anywhere you want
- ... your experiments/simulations/code won't stop when the cleaner unplugs your desktop after you go home

The most useful resource is the [CERN Batch Docs](https://batchdocs.web.cern.ch/index.html) which provide many tutorials. This quick-start will be oriented towards getting a simple Python script running in a Docker container.

All files mentioned are available in this repo, and the Docker image used is available at [registry.cern.ch/tbass/demo:v0.1](https://registry.cern.ch/harbor/projects/3576/repositories/demo/artifacts-tab).

## Docker

Docker is a virtualisation software which creates *Containers*. These function similarly to Virtual Machines - they have their own disks (File System) - but are considerably more lightweight as they share the OS (kernel) with the host server (machine running the Container).

Containers use *Images* as a "starting point" for the virtual machine's file system. The *Dockerfile* describes how the Image is created. The Images you create can be shared using a Registry - we will use CERN's registry (registry.cern.ch).

This means that you can use a predictable, isolated environment to both test your code locally, and run it on high-throughput infrastructure. You don't need to install anything onto the infrastructure, you simply bundle your dependencies into the Image, *Push* it to a registry and *Pull* it to any machine you want to use.

To install Docker on a Mac with Homebrew, simply use `brew install --cask docker`. For other platforms, refer to https://docs.docker.com/get-docker/.

### Dockerfile

Our Dockerfile will use an Nvidia Cuda image as the base. This allows the image to talk to GPUs on HTCondor.

```docker
# Dockerfile
# use an Ubuntu+CUDA official image as base
FROM nvidia/cuda:11.4.2-devel-ubuntu20.04
```

We will then set the timezone to CERN's timezone, update packages with apt-get, and install git, build tools, python3, and cmake

```docker
ENV TZ=Europe/Zurich

RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && \
    echo $TZ > /etc/timezone && \
    apt-get update && apt-get install -y --no-install-recommends \
    git \
    build-essential \
    cmake \
    python3 python3-dev python3-pip && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*
```

Running these commands as a single `RUN` argument compresses them into a single *Layer*. This layer creates a "checkpoint" of the filesystem. If we change following commands, but leave this `RUN` unchanged, we don't need to re-build the entire image. This saves a lot of time!

Next we create a `requirements.txt` for pip to install all the Python packages we need. You can comment out or change versions as you wish, but these packages will allow us to run GPU-accelerated Xsuite simulations and save the results as Parquet or HDF files.

```python
numpy==1.23.5
scipy==1.9.3
pandas==1.5.2
tqdm==4.65.0
matplotlib==3.6.2
xsuite==0.5.0
cpymad==1.10.0
cupy-cuda114==10.6.0
tables==3.8.0
fastparquet==2023.4.0
```

We then add another line to the Dockerfile to copy in the requirements file from our dick and install the packages

```docker
COPY requirements.txt requirements.txt
RUN pip install --no-cache-dir -r requirements.txt
```

Finally we do some cleaning up to set the environment variables for Cupy

```docker
ENV CUPY_CACHE_DIR /tmp/cupy_cache
```

### Building the Image

If you have Docker installed and running, you should get a response similar to the following

```sh
$ docker info

Client:
 Context:    desktop-linux
 Debug Mode: false
...
```

If Docker is running, you can now log in to the Harbor Container Registry. Navigate to https://registry.cern.ch/, log in via OIDC (CERN Oauth), click your name in the top right, select User Profile, and copy your CLI secret to the clipboard. Then run

```sh
$ docker login registry.cern.ch
```

providing your CERN username and CLI secret as the password. Once logged in, create a new Public Repository on the registry (New Project), optionally using your CERN username as the project name.

Now, you can build the Docker image. This will take a while the first time around, and may use up to 6GB of disk space.

The following command builds the Docker image defined in the Dockerfile in your current directory for a Linux machine, ready to push to the repository `PROJECT/REPO`, where PROJECT may be your username, and REPO may be a short description of the project (for instance, `tbass/demo`) with the *Version* tag `0.1`.

```sh
$ docker build . -t registry.cern.ch/PROJECT/REPO:v0.1 --platform linux/amd64
[+] Building 239.3s (10/10) FINISHED
 => [internal] load build definition from Dockerfile
 ...
 => => naming to registry.cern.ch/PROJECT/REPO:v0.1
```

This Docker image now exists on your local machine. To make it available to anyone, use:

```sh
$ docker push registry.cern.ch/PROJECT/REPO:v0.1
aaa000aaa000: Preparing
...
fff999fff999: Preparing
```

## Python Script

Now we want to write some Python to run. We'll create `demo_script.py` and use the basic Xsuite example found [here](https://xsuite.readthedocs.io/en/latest/singlepart.html). We'll save the data as a Parquet file, but you could use any `pandas.DataFrame.to_*` function. We'll also write this under the function `run_simulation()`. This allows us to specify the number of turns and particles each time as a command-line argument, and gives space to run some preamble code (such as importing a MAD-X lattice) in the space above the def.

```python
import numpy as np
import pandas as pd
import xobjects as xo
import xtrack as xt
import xpart as xp
import sys

def run_simulation():
    args = sys.argv[1:]
    n_turns = args[0]
    n_part = args[1]
    line = xt.Line(
        elements=[xt.Drift(length=2.),
                xt.Multipole(knl=[0, 1.], ksl=[0,0]),
                xt.Drift(length=1.),
                xt.Multipole(knl=[0, -1.], ksl=[0,0])],
        element_names=['drift_0', 'quad_0', 'drift_1', 'quad_1'])

    line.particle_ref = xp.Particles(p0c=6500e9, #eV
                                    q0=1, mass0=xp.PROTON_MASS_EV)

    context = xo.ContextCpu()         # For CPU
    line.build_tracker(_context=context)

    particles = xp.Particles(p0c=6500e9, #eV
                            q0=1, mass0=xp.PROTON_MASS_EV,
                            x=np.random.uniform(-1e-3, 1e-3, n_part),
                            px=np.random.uniform(-1e-5, 1e-5, n_part),
                            y=np.random.uniform(-2e-3, 2e-3, n_part),
                            py=np.random.uniform(-3e-5, 3e-5, n_part),
                            zeta=np.random.uniform(-1e-2, 1e-2, n_part),
                            delta=np.random.uniform(-1e-4, 1e-4, n_part),
                            _context=context)

    line.track(particles, num_turns=n_turns,
                turn_by_turn_monitor=True)

    particles.to_pandas().to_parquet(f"{n_turn}_{n_part}_particles.parquet")
```

To specify the number of turns and particles for each simulation run, we create a small tab-deliminated text file `args.txt`, a new line for each simulation run, and the two parameters separated by tabs `TURNS PARTS`:

```
500 500
500 1000
500 2000
```

This runs three simulations, each with 500 turns, for 500, 1000, and 2000 particles. These simulations are hardly intense, but with hundreds of thousands of turns and millions of particles, this simulation could take hours or days, and the `particles.parquet` file could be gigabytes.

## Testing Docker Images
To test a Docker image, you can run the following command to create a *Container* based on the image, and interact with its bash shell

```sh
$ docker run -it --entrypoint bash registry.cern.ch/PROJECT/REPO:v0.1
root@a0a0b1b1c2c2:/# 
```

With this terminal still open, you can use another terminal to check the container status

```sh
$ docker container ls
```
```
CONTAINER ID   IMAGE                                COMMAND   CREATED         STATUS         PORTS     NAMES
a0a0b1b1c2c2   registry.cern.ch/PROJECT/REPO:v0.1   "bash"    7 seconds ago   Up 6 seconds             random_name
```

You can use the following command to copy the python script into the running container (replacing a0a0b1b1c2c2) with your container ID

```sh
$ docker cp demo_script.py a0a0b1b1c2c2:/
```

Now you can return to the container and test the script

```sh
root@a0a0b1b1bc2c2:/# python3 -c "import demo_script; print(demo_script.__name__)"
demo_script
```

## Shell Script

To run our tested python script on a VM, we create a very simple shell script `script.sh` to start it running. This takes any stdin arguments, and passes them to the `run_simulation()` method of the script.

```shell
#!/bin/bash
python3 -c 'import script_demo; script_demo.run_simulation()' "$@"
```

## Submission File

Now, we need to tell HTCondor what to do with our script. We create a `job.sub` file defining this.

First, we tell it to run on the Docker universe, and pull the image we created earlier.

```
universe                = docker
docker_image            = registry.cern.ch/PROJECT/REPO:v0.1
```

Next, we tell it what script to execute when it spawns a new VM, and which arguments to pass it.

```
executable              = script.sh
arguments               = $(turns) $(parts)
```

Next, make the directories `out/`, `err/`, and `log/` in your current directory. HTCondor will copy outputs, errors, and logs into these folders.

```
output			        = out/$(ClusterId).$(ProcId).out
error			        = err/$(ClusterId).$(ProcId).err
log			            = log/$(CluterId).log
```

Next, we tell it to transfer the python script into our docker image. You could include the script into your docker image so that its already there when pulled, but you'd have to re-build your image every time you change your script. The image should be the *environment*, not the process.

```
should_transfer_files   = YES
transfer_input_files    = demo_script.py
```

Next we tell it to give us the output files back when it's finished.

```
when_to_transfer_output = ON_EXIT
transfer_output_files   = $(turns)_$(parts)_particles.parquet
```

Finally, we tell it to queue a job for each line in `args.txt`

```
queue turns, parts from args.txt
```

To get the files back, you need to run this from an LXPlus machine on a directory on AFS. I tend to run mine from `/afs/cern.ch/work/u/username/public`

Lastly, to send the jobs over to HTCondor, run the following command from any LXPlus terminal.

```sh
$ condor_submit job.sub -batch-name job:v0.1
Submitting job(s)...
3 job(s) submitted to cluster 000001.
```

You can check the status of your jobs via command line 
```sh
$ condor_q

-- Schedd: bigbird23.cern.ch : <137.138.55.115:9618?... @ 05/04/23 10:50:38
OWNER BATCH_NAME      SUBMITTED   DONE   RUN    IDLE  TOTAL JOB_IDS
USER job:v0.1       5/4  10:50      _      _      3      3 123456.0-2

Total for query: 43 jobs; 0 completed, 0 removed, 3 idle, 40 running, 0 held, 0 suspended
```

Or via the Grafana monitoring website at https://monit-grafana.cern.ch/d/000000869/user-batch-jobs, selecting your username at the top.

## GPU

To request GPUs from HTCondor, add this line to your `.sub` file:

```
request_gpus            = 1

```

To enable GPU acceleration in Xsuite, you should follow [Xsuite's GPU documentation](https://xsuite.readthedocs.io/en/latest/installation.html#gpu-support). You mostly need to choose the Cupy context:

```python
    context = xo.ContextCupy()         # For Nvidia GPU
```

## Common Issues

*I've tried pushing to the Registry but it tells me to log in.*

```unauthorized: unauthorized to access repository: PROJECT/REPO, action: push: unauthorized to access repository: PROJECT/REPO, action: push```

Go to the registry, copy your CLI secret again, and use `docker login registry.cern.ch` to re-authenticate

*I've done this, but get this error:*

```Error response from daemon: Get "https://registry.cern.ch/v2/": unauthorized: authentication required```

Your CERN OIDC has expired; refresh registry.cern.ch, log-in via OIDC, and log back in to your CERN account. Now repeat the process above.