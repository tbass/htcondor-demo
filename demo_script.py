import numpy as np
import pandas as pd
import xobjects as xo
import xtrack as xt
import xpart as xp
import sys

def run_simulation():
    args = sys.argv[1:]
    n_turns = args[0]
    n_part = args[1]
    line = xt.Line(
        elements=[xt.Drift(length=2.),
                xt.Multipole(knl=[0, 1.], ksl=[0,0]),
                xt.Drift(length=1.),
                xt.Multipole(knl=[0, -1.], ksl=[0,0])],
        element_names=['drift_0', 'quad_0', 'drift_1', 'quad_1'])

    line.particle_ref = xp.Particles(p0c=6500e9, #eV
                                    q0=1, mass0=xp.PROTON_MASS_EV)

    context = xo.ContextCpu()         # For CPU
    line.build_tracker(_context=context)

    particles = xp.Particles(p0c=6500e9, #eV
                            q0=1, mass0=xp.PROTON_MASS_EV,
                            x=np.random.uniform(-1e-3, 1e-3, n_part),
                            px=np.random.uniform(-1e-5, 1e-5, n_part),
                            y=np.random.uniform(-2e-3, 2e-3, n_part),
                            py=np.random.uniform(-3e-5, 3e-5, n_part),
                            zeta=np.random.uniform(-1e-2, 1e-2, n_part),
                            delta=np.random.uniform(-1e-4, 1e-4, n_part),
                            _context=context)

    line.track(particles, num_turns=n_turns,
                turn_by_turn_monitor=True)

    particles.to_pandas().to_parquet(f"{n_turn}_{n_part}_particles.parquet")